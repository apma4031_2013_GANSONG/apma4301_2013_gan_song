"""
poisson.py  -- solve the Poisson problem u_{xx} + u_{yy} = f(x,y)
                on [a,b] x [a,b].
                
                using python sparse linear algebra

     The 5-point Laplacian is used at interior grid points.
     This system of equations is then solved using scipy.sparse.linalg spsolve
     which defaults to umfpack

     code modified from poisson.py from 
     http://www.amath.washington.edu/~rjl/fdmbook/chapter3  (2007)
"""
import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
from mpl_toolkits.mplot3d import Axes3D

def f(x,y):
    
    return 5/4.*np.exp(x+y/2)
def u(x,y):
    return np.exp(x+y/2)
    

def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """

  
    return h*np.linalg.norm(f, 2)
    
def plotconvergence(N_ar,abs_err_ar,rel_err_ar):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    N_ar=[17,33,65,129,257]
    hx = 2./np.array(N_ar)
    hy = 1./np.array(N_ar)
    h = np.sqrt(hx*hy)
    abs_err = np.array(abs_err_ar)
    rel_err = np.array(rel_err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(abs_err),1)
    q = np.polyfit(np.log(h),np.log(rel_err),1)
    pylab.figure()
    pylab.loglog(h,abs_err,'bo-',h,rel_err,'ro-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.9}".format(p[0]))
    pylab.legend(["abs_err","rel_error","best-fit p"],loc="best")
    pylab.grid()
   # pylab.show(block=True)
    pylab.show(block=False)
    print 'coefficients of polyfit h vs abs_err= ' ,p
    print 'coefficients of polyfit h vs rel_err= ' ,q



N_ar=[16,32,64,128,256]
abs_err_ar= np.zeros((5, 1), dtype=float)
rel_err_ar= np.zeros((5, 1), dtype=float)
l=0
for m in N_ar:

    a = 0.0
    b = 1.0
    c = 2.0
                    
  # number of interior points in each direction
    hy = (b-a)/(m+1)
    hx = (c-a)/(m+1)
    x = np.linspace(a,c,m+2)   # grid points x including boundaries
    y = np.linspace(a,b,m+2)   # grid points y including boundaries
    loop_x=np.linspace(0,m-1,m)
    bc_loop=np.linspace(0,m+1,m+2)
    mx = m
    my = m



    X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values
    X=X.T
    Y=Y.T    
    
    F = np.zeros((m*m,1))
    utrue=np.zeros((m+2,m+2))
    rhs=np.zeros((m,m))
    usoln=np.zeros((m+2,m+2))
    t=0


    Xint = X[1:-1,1:-1]        # interior points
    Yint = Y[1:-1,1:-1]


    
    for N in loop_x:
        for M in loop_x:
            xx = Xint[N,M]
            yy = Yint[N,M]        
            rhs[N,M]=f(xx,yy)
       
        
    for N in bc_loop:
        for M in bc_loop:  
            xx = X[N,M]
            yy = Y[N,M]
            usoln[N,M]=u(xx,yy)
            
    rhs[:,0]-= usoln[1:-1,0] / (hy**2)
    rhs[:,-1]-= usoln[1:-1,-1] / (hy**2)
    rhs[0,:]-= usoln[0,1:-1] / (hx**2)
    rhs[-1,:]-= usoln[-1,1:-1] / (hx**2)
    rhs=rhs.T
    
    F = rhs.reshape((mx*my,1))
    I1 = sp.eye(mx,mx) / hy**2
    I2 = sp.eye(my,my)
    e1 = np.ones(mx) / hx**2
    e2 = np.ones(my)
    e3 = np.ones(mx) / hy**2
    T = sp.spdiags([e1,-2.*e1-2.*e3,e1],[-1,0,1],mx,mx) 
    S = sp.spdiags([e2,e2],[-1,1],my,my)
    A = sp.kron(I2,T) + sp.kron(S,I1)
    A = A.tocsr()
    uvec = spsolve(A, F)
    usoln[1:-1, 1:-1] = uvec.reshape( (my,mx) ).T  
    


# Solve the linear system:
     
    
      
    
    
    
    show_result = True
    if show_result:
# plot results:
       pylab.figure()
       ax = Axes3D(pylab.gcf())
       ax.plot_surface(X,Y,usoln, rstride=1, cstride=1, cmap=pylab.cm.jet)
       ax.set_xlabel('t')
       ax.set_ylabel('x')
       ax.set_zlabel('u')
    #pylab.axis([a, b, a, b])
    #pylab.daspect([1 1 1])
       pylab.title('Surface plot of computed solution')

 

    for N in bc_loop:
        for M in bc_loop:
            xx = X[N,M]
            yy = Y[N,M]
            utrue[N,M]=u(xx,yy)        
            
        
    h=np.sqrt(hx*hy)   
    err = utrue-usoln
    abs_err = grid_norm2(err,h)
    rel_err = abs_err/grid_norm2(usoln,h)
    rel_err_ar[l]=rel_err
    abs_err_ar[l]=abs_err

    print 'abs_err=', abs_err, 'rel_err=',rel_err
    l=l+1

plotconvergence(N_ar,abs_err_ar,rel_err_ar)
"""  abs_err_ar[p]= abs_err
     rel_err_ar[p]= rel_err  
     p=p+1
    
"""
pylab.show()

    