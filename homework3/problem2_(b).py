"""
poisson.py  -- solve the Poisson problem u_{xx} + u_{yy} = f(x,y)
                on [a,b] x [a,b].
                
                using python sparse linear algebra

     The 5-point Laplacian is used at interior grid points.
     This system of equations is then solved using scipy.sparse.linalg spsolve
     which defaults to umfpack

     code modified from poisson.py from 
     http://www.amath.washington.edu/~rjl/fdmbook/chapter3  (2007)
"""
import pdb
import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
from scipy.linalg import norm 
from mpl_toolkits.mplot3d import Axes3D

def f(x,y):
    
    return 5/4.*np.exp(x+y/2)
def u(x,y):
    return np.exp(x+y/2)
    

def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """

  
    return norm(f,2)*h
    
    
def plotconvergence(N_ar,abs_err_ar,rel_err_ar):
    """ 
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    N_ar=[17,33,65,129,257]
    h = 1./np.array(N_ar)
    
    abs_err = np.array(abs_err_ar)
    rel_err = np.array(rel_err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    
    p = np.polyfit(np.log(h),np.log(abs_err),1)
    q = np.polyfit(np.log(h),np.log(rel_err),1)
    
    pylab.figure()
    pylab.loglog(h,abs_err,'bo-',h,rel_err,'ro-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.9}".format(p))
    pylab.legend(["abs_err","rel_error","best-fit p"],loc="best")
    pylab.grid()
   # pylab.show(block=True)
    pylab.show(block=False)
    print 'coefficients of polyfit h vs abs_err= ' ,p
    print 'coefficients of polyfit h vs rel_err= ' ,q
    



N_ar=[16,32,64,128,256]
abs_err_ar= np.zeros((5, 1), dtype=float)
rel_err_ar= np.zeros((5, 1), dtype=float)
l=0
for m in N_ar:

    a = 0.0
    b = 1.0                    
  # number of interior points in each direction
    h = (b-a)/(m+1)
    x = np.linspace(a,b,m+2)   # grid points x including boundaries
    y = np.linspace(a,b,m+2)   # grid points y including boundaries
    loop_x=np.linspace(0,m-1,m)
    bc_loop=np.linspace(0,m+1,m+2)




    X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values

    F = np.zeros((m*m,1))
    utrue=np.zeros((m+2,m+2))
    rhs=np.zeros((m,m))
    usoln=np.zeros((m+2,m+2))
   
    Xint = X[1:-1,1:-1]        # interior points
    Yint = Y[1:-1,1:-1]


    
    for N in loop_x:
        for M in loop_x:
            xx = Xint[N,M]
            yy = Yint[N,M]        
            rhs[N,M]=f(xx,yy)
       
         
    for N in bc_loop:
        for M in bc_loop:  
            xx = X[N,M]
            yy = Y[N,M]
            usoln[N,M]=u(xx,yy)
    
             
    rhs[:,0]-= usoln[1:-1,0] / h**2
    rhs[:,-1]-= usoln[1:-1,-1] / h**2
    rhs[0,:]-= usoln[0,1:-1] / h**2
    rhs[-1,:]-= usoln[-1,1:-1] / h**2
    
    F = rhs.reshape((m*m,1)) 
    I = sp.eye(m,m)
    e = np.ones(m)
    T = sp.spdiags([e,-4.*e,e],[-1,0,1],m,m)
    S = sp.spdiags([e,e],[-1,1],m,m)
    A = (sp.kron(I,T) + sp.kron(S,I)) / h**2
    A = A.tocsr()   
    
    show_matrix = False
    if (show_matrix):
        pylab.spy(A,marker='.')

# Solve the linear system:
    uvec = spsolve(A, F)
 

    usoln[1:-1, 1:-1] = uvec.reshape( (m,m) )
    
    show_result = True
    if show_result:
# plot results:
       pylab.figure()
       ax = Axes3D(pylab.gcf())
       ax.plot_surface(X,Y,usoln, rstride=1, cstride=1, cmap=pylab.cm.jet)
       ax.set_xlabel('t')
       ax.set_ylabel('x')
       ax.set_zlabel('u')
     
    #pylab.axis([a, b, a, b])
    #pylab.daspect([1 1 1])
    
       pylab.title('Surface plot of computed solution N=%s'%(m))
    
    
    
    
    for N in bc_loop:
        for M in bc_loop:
            xx = X[N,M]
            yy = Y[N,M]
            utrue[N,M]=u(xx,yy)        
            

    err = utrue-usoln
    abs_err = grid_norm2(err,h)
    rel_err = abs_err/grid_norm2(usoln,h)
    rel_err_ar[l]=rel_err
    abs_err_ar[l]=abs_err
    
   
    print 'abs_err=', abs_err, 'rel_err=',rel_err
    l=l+1

plotconvergence(N_ar,abs_err_ar,rel_err_ar)


pylab.show()
  































































  
    
    
"""
rhs = f(Xint,Yint)         # evaluate f at interior points for right hand side
"""                          # rhs is modified below for boundary conditions.

# set boundary conditions around edges of usoln array:
"""
usoln = np.zeros(X.shape)     # here we just zero everything  
                           # This sets full array, but only boundary values
                           # are used below.  For a problem where utrue
                           # is not known, would have to set each edge of
                           # usoln to the desired Dirichlet boundary values.

"""
# adjust the rhs to include boundary terms: 



# convert the 2d grid function rhs into a column vector for rhs of system:
    


# form matrix A:




    

# reshape vector solution uvec as a grid function and
# insert this interior solution into usoln for plotting purposes:
# (recall boundary conditions in usoln are already set)

    

# using Linf norm of spectral solution good to 10 significant digits
"""
umax_true = 0.07367135328
umax = usoln.max()
abs_err = abs(umax - umax_true)
rel_err = abs_err/umax_true
print "m = {0}".format(m)
print "||u||_inf = {0}, ||u_true||_inf={1}".format(umax,umax_true)
print "Absolute error = {0:10.3e}, relative error = {1:10.3e}".format(abs_err,rel_err)
"""

    
