from fdcoeffV import *

"""
prove the stencils in problem 1

"""


print "Coefficients for first derivative at x=0, stencil [0,1,2]"
print fdcoeffV(1,0,[0,1,2])

print "Coefficients for second derivative at x=0, stencil [0,1,2]"
print fdcoeffV(2,0,[0,1,2])

print "Coefficients for first derivative at x=0.5, stencil [0,1,2]"
print fdcoeffV(1,0.5,[0,1,2])

print "Coefficients for second derivative at x=0.5, stencil [0,1,2]"
print fdcoeffV(2,0.5,[0,1,2])

print "Coefficients for first derivative at x=1, stencil [0,1,2]"
print fdcoeffV(1,1,[0,1,2])

print "Coefficients for second derivative at x=1, stencil [0,1,2]"
print fdcoeffV(2,1,[0,1,2])

#print "Coefficients for fourth derivative at x=0, stencil [-2,-1,0,1,2]"
#print fdcoeffV(4,0,[-2,-1,0,1,2])
