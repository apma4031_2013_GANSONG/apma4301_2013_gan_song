# -*- coding: utf-8 -*-
"""
poisson1d_mms.py:  example program to set up solve poisson equation in 1-D
using manufactured solution u(x) = sin(3 pi x) + x 

   -u_xx = 9*sin(3*pi*x) on x=[0.,1]
   with u(0)= 0., u(1) =1.
   
Created on Tue Sep 18 01:27:13 2012

@author: mspieg
"""

import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import numpy as np
import pylab
from diffMatrix_5points_stencils import setD



def dif_f_exact(k,x):
    """ 
    exact solution for this problem
    sin(3*pi*x)+x
    """
    if k==1:
        return 4.*np.pi*np.cos(4.*np.pi*x) + 2.*x
    
    return -(4.*np.pi)**2*np.sin(4.*np.pi*x) + 2
    
    # return exact solution
    
    
def f_exact(x):
    """
    return rhs for manufactured solution given by u_exact
    """
    return np.sin(4.*np.pi*x) + x**2
    
def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """
    
    return np.sqrt(h)*np.linalg.norm(f, 2)
    

def plotconvergence(N_ar,abs_err_ar,k):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    h = 1./np.array(N_ar)
    abs_err = np.array(abs_err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(abs_err),1)
    pylab.figure()
    pylab.loglog(h,abs_err,'bo-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.3}".format(p[0]))
    pylab.legend(["abs_err","best-fit p"],loc="best")
    pylab.grid()
    pylab.show(block=False)
    print 'p=',p[0]

def main():
    # set number of mesh intervals (mesh points is N+1)
    N_ar = [8,16,32,64,128,256,512,1024]
    # initialize lists for storing output
    abs_err_ar = [0,0,0,0,0,0,0,0]
    K_ar = [1,2]
    t=0
    for K in K_ar:
        for N in N_ar:
        # set numpy grid array to be evenly spaced    
            x = np.linspace(0,1,N+1)
         
        # get full second order second derivative operator
            A = setD(K,x)
        
        # set rhs vector
            f = f_exact(x)
        
        # set solution vector with boundary values 

            f_k_approx = A*f
            
            f_k_exact  = dif_f_exact(K,x)
                 
        # calculate error and errornorm
            h = 1./N
            
            err = f_k_approx - f_k_exact
            abs_err = grid_norm2(err,h)
            
           # rel_err = abs_err/grid_norm2(u_solve,h)
        
       
            print 'N=', N, '   abs_err=', abs_err, #'rel_err=',rel_err
            print
        # collect results for later plotting 
            abs_err_ar[t]= abs_err
            
            t=t+1
            #rel_err_ar.append(rel_err)
        
    # plot it out
    #plotpoisson1d(x,u_solve)    
        plotconvergence(N_ar,abs_err_ar,K)
        t=0
        abs_err_ar=[0,0,0,0,0,0,0,0]
     
    pylab.show()
                
        



if __name__ == "__main__":
    main()
