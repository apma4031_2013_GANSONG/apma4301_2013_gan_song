# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/usr/local/TerraFERMA/share/terraferma/cpp/main.cpp" "/home/gansong/homework4/Problem2b/poisson_p2/build_convergence/CMakeFiles/poisson_convergence.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DOLFIN_VERSION=\"1.2.0+\""
  "BOOST_UBLAS_NDEBUG"
  "HAS_HDF5"
  "_LARGEFILE_SOURCE"
  "_LARGEFILE64_SOURCE"
  "_BSD_SOURCE"
  "_FORTIFY_SOURCE=2"
  "HAS_PETSC"
  "HAS_UMFPACK"
  "HAS_SCOTCH"
  "HAS_PARMETIS"
  "HAS_CGAL"
  "CGAL_DISABLE_ROUNDING_MATH_CHECK"
  "HAS_ZLIB"
  "HAS_CPPUNIT"
  "HAS_MPI"
  "HAS_OPENMP"
  "HAS_QT4"
  "HAS_VTK"
  "HAS_VTK_EXODUS"
  "DOLFIN_VERSION=\"1.2.0+\""
  "BOOST_UBLAS_NDEBUG"
  "HAS_HDF5"
  "_LARGEFILE_SOURCE"
  "_LARGEFILE64_SOURCE"
  "_BSD_SOURCE"
  "_FORTIFY_SOURCE=2"
  "HAS_PETSC"
  "HAS_UMFPACK"
  "HAS_SCOTCH"
  "HAS_PARMETIS"
  "HAS_CGAL"
  "CGAL_DISABLE_ROUNDING_MATH_CHECK"
  "HAS_ZLIB"
  "HAS_CPPUNIT"
  "HAS_MPI"
  "HAS_OPENMP"
  "HAS_QT4"
  "HAS_VTK"
  "HAS_VTK_EXODUS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gansong/homework4/Problem2b/poisson_p2/build_convergence/buckettools_ufc/CMakeFiles/buckettools_ufc.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/TerraFERMA/include"
  "/usr/include/libxml2"
  "/usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/include"
  "/usr/include/scotch"
  "/usr/lib/openmpi/include"
  "/usr/lib/openmpi/include/openmpi"
  "/usr/local/TerraFERMA/src/petsc/include"
  "/usr/include/qt4"
  "/usr/include/vtk-5.8"
  "/usr/include/python2.7"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
