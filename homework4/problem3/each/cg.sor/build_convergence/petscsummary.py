
#------ PETSc Performance Summary ----------

Nproc = 1
Time = [   4.422e+01,]
Objects = [   4.800e+01,]
Flops = [   7.874e+09,]
Memory = [   0.000e+00,]
MPIMessages = [   0.000e+00,]
MPIMessageLengths = [   2.000e+01,]
MPIReductions = [   1.729e+03,]

#Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
#                       Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
#  0:      Main Stage: 4.4215e+01 100.0%  7.8743e+09 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  5.400e+01   3.1% 

# Event
# ------------------------------------------------------
class Stage(object):
    def __init__(self, name, time, flops, numMessages, messageLength, numReductions):
        # The time and flops represent totals across processes, whereas reductions are only counted once
        self.name          = name
        self.time          = time
        self.flops         = flops
        self.numMessages   = numMessages
        self.messageLength = messageLength
        self.numReductions = numReductions
        self.event         = {}
class Dummy(object):
    pass
Main_Stage = Stage('Main_Stage', 44.2146, 7.8743e+09, 0, 0, 54)
#
VecMax = Dummy()
Main_Stage.event['VecMax'] = VecMax
VecMax.Count = [         4,]
VecMax.Time  = [   2.908e-03,]
VecMax.Flops = [   0.000e+00,]
VecMax.NumMessages = [   0.0e+00,]
VecMax.MessageLength = [   0.000e+00,]
VecMax.NumReductions = [   0.0e+00,]
#
VecMin = Dummy()
Main_Stage.event['VecMin'] = VecMin
VecMin.Count = [         4,]
VecMin.Time  = [   2.430e-03,]
VecMin.Flops = [   0.000e+00,]
VecMin.NumMessages = [   0.0e+00,]
VecMin.MessageLength = [   0.000e+00,]
VecMin.NumReductions = [   0.0e+00,]
#
VecTDot = Dummy()
Main_Stage.event['VecTDot'] = VecTDot
VecTDot.Count = [      1536,]
VecTDot.Time  = [   9.903e-01,]
VecTDot.Flops = [   8.085e+08,]
VecTDot.NumMessages = [   0.0e+00,]
VecTDot.MessageLength = [   0.000e+00,]
VecTDot.NumReductions = [   0.0e+00,]
#
VecNorm = Dummy()
Main_Stage.event['VecNorm'] = VecNorm
VecNorm.Count = [       769,]
VecNorm.Time  = [   3.056e-01,]
VecNorm.Flops = [   4.048e+08,]
VecNorm.NumMessages = [   0.0e+00,]
VecNorm.MessageLength = [   0.000e+00,]
VecNorm.NumReductions = [   0.0e+00,]
#
VecCopy = Dummy()
Main_Stage.event['VecCopy'] = VecCopy
VecCopy.Count = [        17,]
VecCopy.Time  = [   1.069e-02,]
VecCopy.Flops = [   0.000e+00,]
VecCopy.NumMessages = [   0.0e+00,]
VecCopy.MessageLength = [   0.000e+00,]
VecCopy.NumReductions = [   0.0e+00,]
#
VecSet = Dummy()
Main_Stage.event['VecSet'] = VecSet
VecSet.Count = [        53,]
VecSet.Time  = [   1.451e-02,]
VecSet.Flops = [   0.000e+00,]
VecSet.NumMessages = [   0.0e+00,]
VecSet.MessageLength = [   0.000e+00,]
VecSet.NumReductions = [   0.0e+00,]
#
VecAXPY = Dummy()
Main_Stage.event['VecAXPY'] = VecAXPY
VecAXPY.Count = [      1537,]
VecAXPY.Time  = [   8.896e-01,]
VecAXPY.Flops = [   8.090e+08,]
VecAXPY.NumMessages = [   0.0e+00,]
VecAXPY.MessageLength = [   0.000e+00,]
VecAXPY.NumReductions = [   0.0e+00,]
#
VecAYPX = Dummy()
Main_Stage.event['VecAYPX'] = VecAYPX
VecAYPX.Count = [       767,]
VecAYPX.Time  = [   7.711e-01,]
VecAYPX.Flops = [   4.037e+08,]
VecAYPX.NumMessages = [   0.0e+00,]
VecAYPX.MessageLength = [   0.000e+00,]
VecAYPX.NumReductions = [   0.0e+00,]
#
VecAssemblyBegin = Dummy()
Main_Stage.event['VecAssemblyBegin'] = VecAssemblyBegin
VecAssemblyBegin.Count = [        23,]
VecAssemblyBegin.Time  = [   5.674e-05,]
VecAssemblyBegin.Flops = [   0.000e+00,]
VecAssemblyBegin.NumMessages = [   0.0e+00,]
VecAssemblyBegin.MessageLength = [   0.000e+00,]
VecAssemblyBegin.NumReductions = [   0.0e+00,]
#
VecAssemblyEnd = Dummy()
Main_Stage.event['VecAssemblyEnd'] = VecAssemblyEnd
VecAssemblyEnd.Count = [        23,]
VecAssemblyEnd.Time  = [   4.339e-05,]
VecAssemblyEnd.Flops = [   0.000e+00,]
VecAssemblyEnd.NumMessages = [   0.0e+00,]
VecAssemblyEnd.MessageLength = [   0.000e+00,]
VecAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatMult = Dummy()
Main_Stage.event['MatMult'] = MatMult
MatMult.Count = [       768,]
MatMult.Time  = [   4.531e+00,]
MatMult.Flops = [   2.621e+09,]
MatMult.NumMessages = [   0.0e+00,]
MatMult.MessageLength = [   0.000e+00,]
MatMult.NumReductions = [   0.0e+00,]
#
MatSOR = Dummy()
Main_Stage.event['MatSOR'] = MatSOR
MatSOR.Count = [       769,]
MatSOR.Time  = [   9.745e+00,]
MatSOR.Flops = [   2.827e+09,]
MatSOR.NumMessages = [   0.0e+00,]
MatSOR.MessageLength = [   0.000e+00,]
MatSOR.NumReductions = [   0.0e+00,]
#
MatAssemblyBegin = Dummy()
Main_Stage.event['MatAssemblyBegin'] = MatAssemblyBegin
MatAssemblyBegin.Count = [         3,]
MatAssemblyBegin.Time  = [   1.788e-05,]
MatAssemblyBegin.Flops = [   0.000e+00,]
MatAssemblyBegin.NumMessages = [   0.0e+00,]
MatAssemblyBegin.MessageLength = [   0.000e+00,]
MatAssemblyBegin.NumReductions = [   0.0e+00,]
#
MatAssemblyEnd = Dummy()
Main_Stage.event['MatAssemblyEnd'] = MatAssemblyEnd
MatAssemblyEnd.Count = [         3,]
MatAssemblyEnd.Time  = [   1.672e-02,]
MatAssemblyEnd.Flops = [   0.000e+00,]
MatAssemblyEnd.NumMessages = [   0.0e+00,]
MatAssemblyEnd.MessageLength = [   0.000e+00,]
MatAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatZeroEntries = Dummy()
Main_Stage.event['MatZeroEntries'] = MatZeroEntries
MatZeroEntries.Count = [         3,]
MatZeroEntries.Time  = [   4.924e-03,]
MatZeroEntries.Flops = [   0.000e+00,]
MatZeroEntries.NumMessages = [   0.0e+00,]
MatZeroEntries.MessageLength = [   0.000e+00,]
MatZeroEntries.NumReductions = [   0.0e+00,]
#
MatView = Dummy()
Main_Stage.event['MatView'] = MatView
MatView.Count = [         1,]
MatView.Time  = [   1.678e-04,]
MatView.Flops = [   0.000e+00,]
MatView.NumMessages = [   0.0e+00,]
MatView.MessageLength = [   0.000e+00,]
MatView.NumReductions = [   0.0e+00,]
#
SNESSolve = Dummy()
Main_Stage.event['SNESSolve'] = SNESSolve
SNESSolve.Count = [         1,]
SNESSolve.Time  = [   2.584e+01,]
SNESSolve.Flops = [   7.874e+09,]
SNESSolve.NumMessages = [   0.0e+00,]
SNESSolve.MessageLength = [   0.000e+00,]
SNESSolve.NumReductions = [   3.0e+00,]
#
SNESFunctionEval = Dummy()
Main_Stage.event['SNESFunctionEval'] = SNESFunctionEval
SNESFunctionEval.Count = [         1,]
SNESFunctionEval.Time  = [   4.065e+00,]
SNESFunctionEval.Flops = [   0.000e+00,]
SNESFunctionEval.NumMessages = [   0.0e+00,]
SNESFunctionEval.MessageLength = [   0.000e+00,]
SNESFunctionEval.NumReductions = [   0.0e+00,]
#
SNESJacobianEval = Dummy()
Main_Stage.event['SNESJacobianEval'] = SNESJacobianEval
SNESJacobianEval.Count = [         1,]
SNESJacobianEval.Time  = [   4.393e+00,]
SNESJacobianEval.Flops = [   0.000e+00,]
SNESJacobianEval.NumMessages = [   0.0e+00,]
SNESJacobianEval.MessageLength = [   0.000e+00,]
SNESJacobianEval.NumReductions = [   0.0e+00,]
#
KSPSetUp = Dummy()
Main_Stage.event['KSPSetUp'] = KSPSetUp
KSPSetUp.Count = [         1,]
KSPSetUp.Time  = [   1.090e-03,]
KSPSetUp.Flops = [   0.000e+00,]
KSPSetUp.NumMessages = [   0.0e+00,]
KSPSetUp.MessageLength = [   0.000e+00,]
KSPSetUp.NumReductions = [   3.0e+00,]
#
KSPSolve = Dummy()
Main_Stage.event['KSPSolve'] = KSPSolve
KSPSolve.Count = [         1,]
KSPSolve.Time  = [   1.738e+01,]
KSPSolve.Flops = [   7.874e+09,]
KSPSolve.NumMessages = [   0.0e+00,]
KSPSolve.MessageLength = [   0.000e+00,]
KSPSolve.NumReductions = [   3.0e+00,]
#
PCSetUp = Dummy()
Main_Stage.event['PCSetUp'] = PCSetUp
PCSetUp.Count = [         1,]
PCSetUp.Time  = [   2.861e-06,]
PCSetUp.Flops = [   0.000e+00,]
PCSetUp.NumMessages = [   0.0e+00,]
PCSetUp.MessageLength = [   0.000e+00,]
PCSetUp.NumReductions = [   0.0e+00,]
#
PCApply = Dummy()
Main_Stage.event['PCApply'] = PCApply
PCApply.Count = [       769,]
PCApply.Time  = [   9.753e+00,]
PCApply.Flops = [   2.827e+09,]
PCApply.NumMessages = [   0.0e+00,]
PCApply.MessageLength = [   0.000e+00,]
PCApply.NumReductions = [   0.0e+00,]
# ========================================================================================================================
AveragetimetogetPetscTime = 0

