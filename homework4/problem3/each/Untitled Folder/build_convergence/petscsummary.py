
#------ PETSc Performance Summary ----------

Nproc = 1
Time = [   4.133e+01,]
Objects = [   5.200e+01,]
Flops = [   5.510e+09,]
Memory = [   0.000e+00,]
MPIMessages = [   0.000e+00,]
MPIMessageLengths = [   2.000e+01,]
MPIReductions = [   1.732e+03,]

#Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
#                       Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
#  0:      Main Stage: 4.1329e+01 100.0%  5.5102e+09 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  5.700e+01   3.3% 

# Event
# ------------------------------------------------------
class Stage(object):
    def __init__(self, name, time, flops, numMessages, messageLength, numReductions):
        # The time and flops represent totals across processes, whereas reductions are only counted once
        self.name          = name
        self.time          = time
        self.flops         = flops
        self.numMessages   = numMessages
        self.messageLength = messageLength
        self.numReductions = numReductions
        self.event         = {}
class Dummy(object):
    pass
Main_Stage = Stage('Main_Stage', 41.3286, 5.51022e+09, 0, 0, 57)
#
VecMax = Dummy()
Main_Stage.event['VecMax'] = VecMax
VecMax.Count = [         4,]
VecMax.Time  = [   2.290e-03,]
VecMax.Flops = [   0.000e+00,]
VecMax.NumMessages = [   0.0e+00,]
VecMax.MessageLength = [   0.000e+00,]
VecMax.NumReductions = [   0.0e+00,]
#
VecMin = Dummy()
Main_Stage.event['VecMin'] = VecMin
VecMin.Count = [         4,]
VecMin.Time  = [   6.754e-03,]
VecMin.Flops = [   0.000e+00,]
VecMin.NumMessages = [   0.0e+00,]
VecMin.MessageLength = [   0.000e+00,]
VecMin.NumReductions = [   0.0e+00,]
#
VecTDot = Dummy()
Main_Stage.event['VecTDot'] = VecTDot
VecTDot.Count = [      1102,]
VecTDot.Time  = [   7.707e-01,]
VecTDot.Flops = [   5.800e+08,]
VecTDot.NumMessages = [   0.0e+00,]
VecTDot.MessageLength = [   0.000e+00,]
VecTDot.NumReductions = [   0.0e+00,]
#
VecNorm = Dummy()
Main_Stage.event['VecNorm'] = VecNorm
VecNorm.Count = [       552,]
VecNorm.Time  = [   2.670e-01,]
VecNorm.Flops = [   2.905e+08,]
VecNorm.NumMessages = [   0.0e+00,]
VecNorm.MessageLength = [   0.000e+00,]
VecNorm.NumReductions = [   0.0e+00,]
#
VecCopy = Dummy()
Main_Stage.event['VecCopy'] = VecCopy
VecCopy.Count = [        17,]
VecCopy.Time  = [   5.785e-03,]
VecCopy.Flops = [   0.000e+00,]
VecCopy.NumMessages = [   0.0e+00,]
VecCopy.MessageLength = [   0.000e+00,]
VecCopy.NumReductions = [   0.0e+00,]
#
VecSet = Dummy()
Main_Stage.event['VecSet'] = VecSet
VecSet.Count = [        53,]
VecSet.Time  = [   1.847e-02,]
VecSet.Flops = [   0.000e+00,]
VecSet.NumMessages = [   0.0e+00,]
VecSet.MessageLength = [   0.000e+00,]
VecSet.NumReductions = [   0.0e+00,]
#
VecAXPY = Dummy()
Main_Stage.event['VecAXPY'] = VecAXPY
VecAXPY.Count = [      1103,]
VecAXPY.Time  = [   7.396e-01,]
VecAXPY.Flops = [   5.806e+08,]
VecAXPY.NumMessages = [   0.0e+00,]
VecAXPY.MessageLength = [   0.000e+00,]
VecAXPY.NumReductions = [   0.0e+00,]
#
VecAYPX = Dummy()
Main_Stage.event['VecAYPX'] = VecAYPX
VecAYPX.Count = [       550,]
VecAYPX.Time  = [   7.155e-01,]
VecAYPX.Flops = [   2.895e+08,]
VecAYPX.NumMessages = [   0.0e+00,]
VecAYPX.MessageLength = [   0.000e+00,]
VecAYPX.NumReductions = [   0.0e+00,]
#
VecAssemblyBegin = Dummy()
Main_Stage.event['VecAssemblyBegin'] = VecAssemblyBegin
VecAssemblyBegin.Count = [        23,]
VecAssemblyBegin.Time  = [   4.673e-05,]
VecAssemblyBegin.Flops = [   0.000e+00,]
VecAssemblyBegin.NumMessages = [   0.0e+00,]
VecAssemblyBegin.MessageLength = [   0.000e+00,]
VecAssemblyBegin.NumReductions = [   0.0e+00,]
#
VecAssemblyEnd = Dummy()
Main_Stage.event['VecAssemblyEnd'] = VecAssemblyEnd
VecAssemblyEnd.Count = [        23,]
VecAssemblyEnd.Time  = [   3.386e-05,]
VecAssemblyEnd.Flops = [   0.000e+00,]
VecAssemblyEnd.NumMessages = [   0.0e+00,]
VecAssemblyEnd.MessageLength = [   0.000e+00,]
VecAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatMult = Dummy()
Main_Stage.event['MatMult'] = MatMult
MatMult.Count = [       551,]
MatMult.Time  = [   3.936e+00,]
MatMult.Flops = [   1.881e+09,]
MatMult.NumMessages = [   0.0e+00,]
MatMult.MessageLength = [   0.000e+00,]
MatMult.NumReductions = [   0.0e+00,]
#
MatSolve = Dummy()
Main_Stage.event['MatSolve'] = MatSolve
MatSolve.Count = [       552,]
MatSolve.Time  = [   5.195e+00,]
MatSolve.Flops = [   1.884e+09,]
MatSolve.NumMessages = [   0.0e+00,]
MatSolve.MessageLength = [   0.000e+00,]
MatSolve.NumReductions = [   0.0e+00,]
#
MatLUFactorNum = Dummy()
Main_Stage.event['MatLUFactorNum'] = MatLUFactorNum
MatLUFactorNum.Count = [         1,]
MatLUFactorNum.Time  = [   4.012e-02,]
MatLUFactorNum.Flops = [   5.089e+06,]
MatLUFactorNum.NumMessages = [   0.0e+00,]
MatLUFactorNum.MessageLength = [   0.000e+00,]
MatLUFactorNum.NumReductions = [   0.0e+00,]
#
MatILUFactorSym = Dummy()
Main_Stage.event['MatILUFactorSym'] = MatILUFactorSym
MatILUFactorSym.Count = [         1,]
MatILUFactorSym.Time  = [   1.693e-02,]
MatILUFactorSym.Flops = [   0.000e+00,]
MatILUFactorSym.NumMessages = [   0.0e+00,]
MatILUFactorSym.MessageLength = [   0.000e+00,]
MatILUFactorSym.NumReductions = [   1.0e+00,]
#
MatAssemblyBegin = Dummy()
Main_Stage.event['MatAssemblyBegin'] = MatAssemblyBegin
MatAssemblyBegin.Count = [         3,]
MatAssemblyBegin.Time  = [   8.106e-06,]
MatAssemblyBegin.Flops = [   0.000e+00,]
MatAssemblyBegin.NumMessages = [   0.0e+00,]
MatAssemblyBegin.MessageLength = [   0.000e+00,]
MatAssemblyBegin.NumReductions = [   0.0e+00,]
#
MatAssemblyEnd = Dummy()
Main_Stage.event['MatAssemblyEnd'] = MatAssemblyEnd
MatAssemblyEnd.Count = [         3,]
MatAssemblyEnd.Time  = [   7.201e-03,]
MatAssemblyEnd.Flops = [   0.000e+00,]
MatAssemblyEnd.NumMessages = [   0.0e+00,]
MatAssemblyEnd.MessageLength = [   0.000e+00,]
MatAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatGetRowIJ = Dummy()
Main_Stage.event['MatGetRowIJ'] = MatGetRowIJ
MatGetRowIJ.Count = [         1,]
MatGetRowIJ.Time  = [   9.537e-07,]
MatGetRowIJ.Flops = [   0.000e+00,]
MatGetRowIJ.NumMessages = [   0.0e+00,]
MatGetRowIJ.MessageLength = [   0.000e+00,]
MatGetRowIJ.NumReductions = [   0.0e+00,]
#
MatGetOrdering = Dummy()
Main_Stage.event['MatGetOrdering'] = MatGetOrdering
MatGetOrdering.Count = [         1,]
MatGetOrdering.Time  = [   6.680e-03,]
MatGetOrdering.Flops = [   0.000e+00,]
MatGetOrdering.NumMessages = [   0.0e+00,]
MatGetOrdering.MessageLength = [   0.000e+00,]
MatGetOrdering.NumReductions = [   2.0e+00,]
#
MatZeroEntries = Dummy()
Main_Stage.event['MatZeroEntries'] = MatZeroEntries
MatZeroEntries.Count = [         3,]
MatZeroEntries.Time  = [   8.490e-03,]
MatZeroEntries.Flops = [   0.000e+00,]
MatZeroEntries.NumMessages = [   0.0e+00,]
MatZeroEntries.MessageLength = [   0.000e+00,]
MatZeroEntries.NumReductions = [   0.0e+00,]
#
MatView = Dummy()
Main_Stage.event['MatView'] = MatView
MatView.Count = [         1,]
MatView.Time  = [   0.000e+00,]
MatView.Flops = [   0.000e+00,]
MatView.NumMessages = [   0.0e+00,]
MatView.MessageLength = [   0.000e+00,]
MatView.NumReductions = [   0.0e+00,]
#
SNESSolve = Dummy()
Main_Stage.event['SNESSolve'] = SNESSolve
SNESSolve.Count = [         1,]
SNESSolve.Time  = [   2.174e+01,]
SNESSolve.Flops = [   5.510e+09,]
SNESSolve.NumMessages = [   0.0e+00,]
SNESSolve.MessageLength = [   0.000e+00,]
SNESSolve.NumReductions = [   6.0e+00,]
#
SNESFunctionEval = Dummy()
Main_Stage.event['SNESFunctionEval'] = SNESFunctionEval
SNESFunctionEval.Count = [         1,]
SNESFunctionEval.Time  = [   4.834e+00,]
SNESFunctionEval.Flops = [   0.000e+00,]
SNESFunctionEval.NumMessages = [   0.0e+00,]
SNESFunctionEval.MessageLength = [   0.000e+00,]
SNESFunctionEval.NumReductions = [   0.0e+00,]
#
SNESJacobianEval = Dummy()
Main_Stage.event['SNESJacobianEval'] = SNESJacobianEval
SNESJacobianEval.Count = [         1,]
SNESJacobianEval.Time  = [   5.102e+00,]
SNESJacobianEval.Flops = [   0.000e+00,]
SNESJacobianEval.NumMessages = [   0.0e+00,]
SNESJacobianEval.MessageLength = [   0.000e+00,]
SNESJacobianEval.NumReductions = [   0.0e+00,]
#
KSPSetUp = Dummy()
Main_Stage.event['KSPSetUp'] = KSPSetUp
KSPSetUp.Count = [         1,]
KSPSetUp.Time  = [   7.730e-04,]
KSPSetUp.Flops = [   0.000e+00,]
KSPSetUp.NumMessages = [   0.0e+00,]
KSPSetUp.MessageLength = [   0.000e+00,]
KSPSetUp.NumReductions = [   3.0e+00,]
#
KSPSolve = Dummy()
Main_Stage.event['KSPSolve'] = KSPSolve
KSPSolve.Count = [         1,]
KSPSolve.Time  = [   1.181e+01,]
KSPSolve.Flops = [   5.510e+09,]
KSPSolve.NumMessages = [   0.0e+00,]
KSPSolve.MessageLength = [   0.000e+00,]
KSPSolve.NumReductions = [   6.0e+00,]
#
PCSetUp = Dummy()
Main_Stage.event['PCSetUp'] = PCSetUp
PCSetUp.Count = [         1,]
PCSetUp.Time  = [   6.382e-02,]
PCSetUp.Flops = [   5.089e+06,]
PCSetUp.NumMessages = [   0.0e+00,]
PCSetUp.MessageLength = [   0.000e+00,]
PCSetUp.NumReductions = [   3.0e+00,]
#
PCApply = Dummy()
Main_Stage.event['PCApply'] = PCApply
PCApply.Count = [       552,]
PCApply.Time  = [   5.200e+00,]
PCApply.Flops = [   1.884e+09,]
PCApply.NumMessages = [   0.0e+00,]
PCApply.MessageLength = [   0.000e+00,]
PCApply.NumReductions = [   0.0e+00,]
# ========================================================================================================================
AveragetimetogetPetscTime = 0

