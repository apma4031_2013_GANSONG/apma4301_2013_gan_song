
#------ PETSc Performance Summary ----------

Nproc = 1
Time = [   4.355e+01,]
Objects = [   4.600e+01,]
Flops = [   7.349e+09,]
Memory = [   0.000e+00,]
MPIMessages = [   0.000e+00,]
MPIMessageLengths = [   2.000e+01,]
MPIReductions = [   1.728e+03,]

#Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
#                       Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
#  0:      Main Stage: 4.3549e+01 100.0%  7.3494e+09 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  5.300e+01   3.1% 

# Event
# ------------------------------------------------------
class Stage(object):
    def __init__(self, name, time, flops, numMessages, messageLength, numReductions):
        # The time and flops represent totals across processes, whereas reductions are only counted once
        self.name          = name
        self.time          = time
        self.flops         = flops
        self.numMessages   = numMessages
        self.messageLength = messageLength
        self.numReductions = numReductions
        self.event         = {}
class Dummy(object):
    pass
Main_Stage = Stage('Main_Stage', 43.5491, 7.34944e+09, 0, 0, 53)
#
VecMax = Dummy()
Main_Stage.event['VecMax'] = VecMax
VecMax.Count = [         4,]
VecMax.Time  = [   1.064e-02,]
VecMax.Flops = [   0.000e+00,]
VecMax.NumMessages = [   0.0e+00,]
VecMax.MessageLength = [   0.000e+00,]
VecMax.NumReductions = [   0.0e+00,]
#
VecMin = Dummy()
Main_Stage.event['VecMin'] = VecMin
VecMin.Count = [         4,]
VecMin.Time  = [   3.508e-03,]
VecMin.Flops = [   0.000e+00,]
VecMin.NumMessages = [   0.0e+00,]
VecMin.MessageLength = [   0.000e+00,]
VecMin.NumReductions = [   0.0e+00,]
#
VecCopy = Dummy()
Main_Stage.event['VecCopy'] = VecCopy
VecCopy.Count = [        15,]
VecCopy.Time  = [   6.366e-03,]
VecCopy.Flops = [   0.000e+00,]
VecCopy.NumMessages = [   0.0e+00,]
VecCopy.MessageLength = [   0.000e+00,]
VecCopy.NumReductions = [   0.0e+00,]
#
VecSet = Dummy()
Main_Stage.event['VecSet'] = VecSet
VecSet.Count = [        52,]
VecSet.Time  = [   1.979e-02,]
VecSet.Flops = [   0.000e+00,]
VecSet.NumMessages = [   0.0e+00,]
VecSet.MessageLength = [   0.000e+00,]
VecSet.NumReductions = [   0.0e+00,]
#
VecAXPY = Dummy()
Main_Stage.event['VecAXPY'] = VecAXPY
VecAXPY.Count = [         1,]
VecAXPY.Time  = [   6.230e-04,]
VecAXPY.Flops = [   5.263e+05,]
VecAXPY.NumMessages = [   0.0e+00,]
VecAXPY.MessageLength = [   0.000e+00,]
VecAXPY.NumReductions = [   0.0e+00,]
#
VecAssemblyBegin = Dummy()
Main_Stage.event['VecAssemblyBegin'] = VecAssemblyBegin
VecAssemblyBegin.Count = [        23,]
VecAssemblyBegin.Time  = [   2.599e-05,]
VecAssemblyBegin.Flops = [   0.000e+00,]
VecAssemblyBegin.NumMessages = [   0.0e+00,]
VecAssemblyBegin.MessageLength = [   0.000e+00,]
VecAssemblyBegin.NumReductions = [   0.0e+00,]
#
VecAssemblyEnd = Dummy()
Main_Stage.event['VecAssemblyEnd'] = VecAssemblyEnd
VecAssemblyEnd.Count = [        23,]
VecAssemblyEnd.Time  = [   1.311e-05,]
VecAssemblyEnd.Flops = [   0.000e+00,]
VecAssemblyEnd.NumMessages = [   0.0e+00,]
VecAssemblyEnd.MessageLength = [   0.000e+00,]
VecAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatSOR = Dummy()
Main_Stage.event['MatSOR'] = MatSOR
MatSOR.Count = [         1,]
MatSOR.Time  = [   1.719e+01,]
MatSOR.Flops = [   7.349e+09,]
MatSOR.NumMessages = [   0.0e+00,]
MatSOR.MessageLength = [   0.000e+00,]
MatSOR.NumReductions = [   0.0e+00,]
#
MatAssemblyBegin = Dummy()
Main_Stage.event['MatAssemblyBegin'] = MatAssemblyBegin
MatAssemblyBegin.Count = [         3,]
MatAssemblyBegin.Time  = [   1.907e-06,]
MatAssemblyBegin.Flops = [   0.000e+00,]
MatAssemblyBegin.NumMessages = [   0.0e+00,]
MatAssemblyBegin.MessageLength = [   0.000e+00,]
MatAssemblyBegin.NumReductions = [   0.0e+00,]
#
MatAssemblyEnd = Dummy()
Main_Stage.event['MatAssemblyEnd'] = MatAssemblyEnd
MatAssemblyEnd.Count = [         3,]
MatAssemblyEnd.Time  = [   1.156e-02,]
MatAssemblyEnd.Flops = [   0.000e+00,]
MatAssemblyEnd.NumMessages = [   0.0e+00,]
MatAssemblyEnd.MessageLength = [   0.000e+00,]
MatAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatZeroEntries = Dummy()
Main_Stage.event['MatZeroEntries'] = MatZeroEntries
MatZeroEntries.Count = [         3,]
MatZeroEntries.Time  = [   9.977e-03,]
MatZeroEntries.Flops = [   0.000e+00,]
MatZeroEntries.NumMessages = [   0.0e+00,]
MatZeroEntries.MessageLength = [   0.000e+00,]
MatZeroEntries.NumReductions = [   0.0e+00,]
#
MatView = Dummy()
Main_Stage.event['MatView'] = MatView
MatView.Count = [         1,]
MatView.Time  = [   7.391e-05,]
MatView.Flops = [   0.000e+00,]
MatView.NumMessages = [   0.0e+00,]
MatView.MessageLength = [   0.000e+00,]
MatView.NumReductions = [   0.0e+00,]
#
SNESSolve = Dummy()
Main_Stage.event['SNESSolve'] = SNESSolve
SNESSolve.Count = [         1,]
SNESSolve.Time  = [   2.508e+01,]
SNESSolve.Flops = [   7.349e+09,]
SNESSolve.NumMessages = [   0.0e+00,]
SNESSolve.MessageLength = [   0.000e+00,]
SNESSolve.NumReductions = [   2.0e+00,]
#
SNESFunctionEval = Dummy()
Main_Stage.event['SNESFunctionEval'] = SNESFunctionEval
SNESFunctionEval.Count = [         1,]
SNESFunctionEval.Time  = [   3.598e+00,]
SNESFunctionEval.Flops = [   0.000e+00,]
SNESFunctionEval.NumMessages = [   0.0e+00,]
SNESFunctionEval.MessageLength = [   0.000e+00,]
SNESFunctionEval.NumReductions = [   0.0e+00,]
#
SNESJacobianEval = Dummy()
Main_Stage.event['SNESJacobianEval'] = SNESJacobianEval
SNESJacobianEval.Count = [         1,]
SNESJacobianEval.Time  = [   4.283e+00,]
SNESJacobianEval.Flops = [   0.000e+00,]
SNESJacobianEval.NumMessages = [   0.0e+00,]
SNESJacobianEval.MessageLength = [   0.000e+00,]
SNESJacobianEval.NumReductions = [   0.0e+00,]
#
KSPSetUp = Dummy()
Main_Stage.event['KSPSetUp'] = KSPSetUp
KSPSetUp.Count = [         1,]
KSPSetUp.Time  = [   8.854e-03,]
KSPSetUp.Flops = [   0.000e+00,]
KSPSetUp.NumMessages = [   0.0e+00,]
KSPSetUp.MessageLength = [   0.000e+00,]
KSPSetUp.NumReductions = [   2.0e+00,]
#
KSPSolve = Dummy()
Main_Stage.event['KSPSolve'] = KSPSolve
KSPSolve.Count = [         1,]
KSPSolve.Time  = [   1.720e+01,]
KSPSolve.Flops = [   7.349e+09,]
KSPSolve.NumMessages = [   0.0e+00,]
KSPSolve.MessageLength = [   0.000e+00,]
KSPSolve.NumReductions = [   2.0e+00,]
#
PCSetUp = Dummy()
Main_Stage.event['PCSetUp'] = PCSetUp
PCSetUp.Count = [         1,]
PCSetUp.Time  = [   0.000e+00,]
PCSetUp.Flops = [   0.000e+00,]
PCSetUp.NumMessages = [   0.0e+00,]
PCSetUp.MessageLength = [   0.000e+00,]
PCSetUp.NumReductions = [   0.0e+00,]
# ========================================================================================================================
AveragetimetogetPetscTime = 1.38044e-05

