# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /usr/local/TerraFERMA/share/terraferma/cpp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/gansong/homework6/ForwardEuler/0.000002/build

# Include any dependencies generated for this target.
include CMakeFiles/diffusion_FowrdEuler.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/diffusion_FowrdEuler.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/diffusion_FowrdEuler.dir/flags.make

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o: CMakeFiles/diffusion_FowrdEuler.dir/flags.make
CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o: /usr/local/TerraFERMA/share/terraferma/cpp/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/gansong/homework6/ForwardEuler/0.000002/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o -c /usr/local/TerraFERMA/share/terraferma/cpp/main.cpp

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /usr/local/TerraFERMA/share/terraferma/cpp/main.cpp > CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.i

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /usr/local/TerraFERMA/share/terraferma/cpp/main.cpp -o CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.s

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.requires:
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.requires

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.provides: CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/diffusion_FowrdEuler.dir/build.make CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.provides.build
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.provides

CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.provides.build: CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o

# Object files for target diffusion_FowrdEuler
diffusion_FowrdEuler_OBJECTS = \
"CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o"

# External object files for target diffusion_FowrdEuler
diffusion_FowrdEuler_EXTERNAL_OBJECTS =

diffusion_FowrdEuler: CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o
diffusion_FowrdEuler: CMakeFiles/diffusion_FowrdEuler.dir/build.make
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libdolfin.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libxml2.so
diffusion_FowrdEuler: /usr/lib/libarmadillo.so
diffusion_FowrdEuler: /usr/lib/liblapack.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libboost_filesystem-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_program_options-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_system-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_thread-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_iostreams-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_mpi-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_serialization-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_timer-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_chrono-mt.so
diffusion_FowrdEuler: /usr/lib/libhdf5.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libpthread.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libz.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/librt.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libm.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libcholmod.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
diffusion_FowrdEuler: /usr/lib/libcamd.so
diffusion_FowrdEuler: /usr/lib/libcolamd.so
diffusion_FowrdEuler: /usr/lib/libccolamd.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libparmetis.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libmetis.so
diffusion_FowrdEuler: /usr/lib/liblapack.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/gcc/x86_64-linux-gnu/4.7/libgfortran.so
diffusion_FowrdEuler: /usr/lib/gcc/x86_64-linux-gnu/4.7/libgfortran.so
diffusion_FowrdEuler: /usr/lib/libptscotch.so
diffusion_FowrdEuler: /usr/lib/libptesmumps.so
diffusion_FowrdEuler: /usr/lib/libptscotcherr.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libparmetis.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libmetis.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libCGAL.so
diffusion_FowrdEuler: /usr/lib/libboost_thread-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_system-mt.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libgmp.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libmpfr.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libz.so
diffusion_FowrdEuler: /usr/lib/libcppunit.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi_cxx.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-rte.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-pal.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libdl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libnsl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libutil.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libm.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libdl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libQtGui.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libQtCore.so
diffusion_FowrdEuler: /usr/lib/libvtkCommon.so
diffusion_FowrdEuler: /usr/lib/libvtkFiltering.so
diffusion_FowrdEuler: /usr/lib/libvtkImaging.so
diffusion_FowrdEuler: /usr/lib/libvtkGraphics.so
diffusion_FowrdEuler: /usr/lib/libvtkGenericFiltering.so
diffusion_FowrdEuler: /usr/lib/libvtkIO.so
diffusion_FowrdEuler: /usr/lib/libvtkRendering.so
diffusion_FowrdEuler: /usr/lib/libvtkVolumeRendering.so
diffusion_FowrdEuler: /usr/lib/libvtkHybrid.so
diffusion_FowrdEuler: /usr/lib/libvtkWidgets.so
diffusion_FowrdEuler: /usr/lib/libvtkParallel.so
diffusion_FowrdEuler: /usr/lib/libvtkInfovis.so
diffusion_FowrdEuler: /usr/lib/libvtkGeovis.so
diffusion_FowrdEuler: /usr/lib/libvtkViews.so
diffusion_FowrdEuler: /usr/lib/libvtkCharts.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libbuckettools_cpp.so
diffusion_FowrdEuler: buckettools_ufc/libbuckettools_ufc.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libspud.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libpython2.7.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libdolfin.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libxml2.so
diffusion_FowrdEuler: /usr/lib/libarmadillo.so
diffusion_FowrdEuler: /usr/lib/liblapack.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libboost_filesystem-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_program_options-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_system-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_thread-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_iostreams-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_mpi-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_serialization-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_timer-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_chrono-mt.so
diffusion_FowrdEuler: /usr/lib/libhdf5.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libpthread.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libz.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/librt.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libm.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
diffusion_FowrdEuler: /usr/lib/libcholmod.so
diffusion_FowrdEuler: /usr/lib/libcamd.so
diffusion_FowrdEuler: /usr/lib/libcolamd.so
diffusion_FowrdEuler: /usr/lib/libccolamd.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libparmetis.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libmetis.so
diffusion_FowrdEuler: /usr/lib/gcc/x86_64-linux-gnu/4.7/libgfortran.so
diffusion_FowrdEuler: /usr/lib/libptscotch.so
diffusion_FowrdEuler: /usr/lib/libptesmumps.so
diffusion_FowrdEuler: /usr/lib/libptscotcherr.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libCGAL.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libgmp.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libmpfr.so
diffusion_FowrdEuler: /usr/lib/libcppunit.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi_cxx.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-rte.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-pal.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libdl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libnsl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libutil.so
diffusion_FowrdEuler: /usr/lib/liblapack.so
diffusion_FowrdEuler: /usr/lib/libcblas.so
diffusion_FowrdEuler: /usr/lib/libf77blas.so
diffusion_FowrdEuler: /usr/lib/libatlas.so
diffusion_FowrdEuler: /usr/lib/libboost_filesystem-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_program_options-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_system-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_thread-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_iostreams-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_mpi-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_serialization-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_timer-mt.so
diffusion_FowrdEuler: /usr/lib/libboost_chrono-mt.so
diffusion_FowrdEuler: /usr/lib/libhdf5.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libpthread.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libz.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/librt.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libm.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
diffusion_FowrdEuler: /usr/local/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
diffusion_FowrdEuler: /usr/lib/libcholmod.so
diffusion_FowrdEuler: /usr/lib/libcamd.so
diffusion_FowrdEuler: /usr/lib/libcolamd.so
diffusion_FowrdEuler: /usr/lib/libccolamd.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libparmetis.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libmetis.so
diffusion_FowrdEuler: /usr/lib/gcc/x86_64-linux-gnu/4.7/libgfortran.so
diffusion_FowrdEuler: /usr/lib/libptscotch.so
diffusion_FowrdEuler: /usr/lib/libptesmumps.so
diffusion_FowrdEuler: /usr/lib/libptscotcherr.so
diffusion_FowrdEuler: /usr/local/TerraFERMA/lib/libCGAL.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libgmp.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libmpfr.so
diffusion_FowrdEuler: /usr/lib/libcppunit.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi_cxx.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libmpi.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-rte.so
diffusion_FowrdEuler: /usr/lib/openmpi/lib/libopen-pal.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libdl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libnsl.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libutil.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libQtGui.so
diffusion_FowrdEuler: /usr/lib/x86_64-linux-gnu/libQtCore.so
diffusion_FowrdEuler: /usr/lib/libvtkCommon.so
diffusion_FowrdEuler: /usr/lib/libvtkFiltering.so
diffusion_FowrdEuler: /usr/lib/libvtkImaging.so
diffusion_FowrdEuler: /usr/lib/libvtkGraphics.so
diffusion_FowrdEuler: /usr/lib/libvtkGenericFiltering.so
diffusion_FowrdEuler: /usr/lib/libvtkIO.so
diffusion_FowrdEuler: /usr/lib/libvtkRendering.so
diffusion_FowrdEuler: /usr/lib/libvtkVolumeRendering.so
diffusion_FowrdEuler: /usr/lib/libvtkHybrid.so
diffusion_FowrdEuler: /usr/lib/libvtkWidgets.so
diffusion_FowrdEuler: /usr/lib/libvtkParallel.so
diffusion_FowrdEuler: /usr/lib/libvtkInfovis.so
diffusion_FowrdEuler: /usr/lib/libvtkGeovis.so
diffusion_FowrdEuler: /usr/lib/libvtkViews.so
diffusion_FowrdEuler: /usr/lib/libvtkCharts.so
diffusion_FowrdEuler: CMakeFiles/diffusion_FowrdEuler.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable diffusion_FowrdEuler"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/diffusion_FowrdEuler.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/diffusion_FowrdEuler.dir/build: diffusion_FowrdEuler
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/build

CMakeFiles/diffusion_FowrdEuler.dir/requires: CMakeFiles/diffusion_FowrdEuler.dir/main.cpp.o.requires
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/requires

CMakeFiles/diffusion_FowrdEuler.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/diffusion_FowrdEuler.dir/cmake_clean.cmake
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/clean

CMakeFiles/diffusion_FowrdEuler.dir/depend:
	cd /home/gansong/homework6/ForwardEuler/0.000002/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /usr/local/TerraFERMA/share/terraferma/cpp /usr/local/TerraFERMA/share/terraferma/cpp /home/gansong/homework6/ForwardEuler/0.000002/build /home/gansong/homework6/ForwardEuler/0.000002/build /home/gansong/homework6/ForwardEuler/0.000002/build/CMakeFiles/diffusion_FowrdEuler.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/diffusion_FowrdEuler.dir/depend

